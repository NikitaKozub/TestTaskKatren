﻿using System.Windows.Controls;
using ViewModel;

namespace View
{
    /// <summary>
    /// Логика взаимодействия для StatisticsWindow.xaml
    /// </summary>
    public partial class StatisticsNoSensitiveWindow : UserControl
    {
        public StatisticsNoSensitiveWindow()
        {
            InitializeComponent();
            DataContext = new StatisticsNoSensitiveViewModel();
        }
    }
}
