﻿using System.Windows.Controls;
using ViewModel;

namespace View
{
    /// <summary>
    /// Логика взаимодействия для StatisticsSensitiveWindows.xaml
    /// </summary>
    public partial class StatisticsSensitiveWindows : UserControl
    {
        public StatisticsSensitiveWindows()
        {
            InitializeComponent();
            DataContext = new StatisticsSensitiveViewModel();
        }
    }
}
